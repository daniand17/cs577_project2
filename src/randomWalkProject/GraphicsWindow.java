package randomWalkProject;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraphicsWindow extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JFrame window;

	private PPMFrame ppmframe;

	public GraphicsWindow(int width, int height) {
		window = new JFrame("PPM Viewer");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);

		window.setPreferredSize(new Dimension(width, height));
		window.setLocationRelativeTo(null);

		ppmframe = new PPMFrame(width, height);
		window.add(ppmframe);

		window.pack();
		window.setVisible(true);
	}

	public void showPPM(String filename) throws FileNotFoundException {

		File infile = new File(filename + ".ppm");
		Scanner scanner = new Scanner(infile);
		// Advance past header
		scanner.nextLine();
		String[] dims = scanner.nextLine().split(" ");
		int width = Integer.parseInt(dims[0]);
		int height = Integer.parseInt(dims[1]);
		scanner.nextLine();

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				String[] tokens = scanner.nextLine().split(" ");

				Color newColor = new Color(Integer.parseInt(tokens[0]),
						Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));

			}
		}
	}
}

class PPMFrame extends JPanel {

	private static final long serialVersionUID = 1L;

	private Color[][] colorArray;

	private boolean paintComponent = true;

	void showPicture() {
		paintComponent = true;
	}

	public PPMFrame(int width, int height) {
		colorArray = new Color[width][height];
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				colorArray[i][j] = Color.white;
	}

	protected void paintComponent(Graphics g) {
		if ( !paintComponent )
			return;
		for (int i = 0; i < colorArray.length; i++) {
			for (int j = 0; j < colorArray[0].length; j++) {
				g.setColor(colorArray[i][j]);
				g.drawRect(i, j, 2, 2);
			}
		}
	}

	void drawPixel(int x, int y, Color color) {
		colorArray[x][y] = color;
	}
}