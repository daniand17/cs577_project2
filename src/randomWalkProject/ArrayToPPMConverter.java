package randomWalkProject;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * This class takes an array of floats normalized to the maximum value and
 * converts that data to a PPM file format to be viewed using any program which
 * can view the PPM file format (ie GIMP).
 * 
 * @author Andrew Daniel
 *
 */
public class ArrayToPPMConverter {

	// The pixel array which will be converted
	private PixelArray pixArrayToConvert;
	// The print writer which will write to a ppm file
	private PrintWriter pw;

	/**
	 * Public constructor which takes a PixelArray and a filename to write to
	 * 
	 * @param pixArray
	 *            the PixelArray to convert
	 * @param filename
	 *            the name of the file to write to
	 */
	public ArrayToPPMConverter(PixelArray pixArray, String filename) {

		pixArrayToConvert = pixArray;
		// Create the PrintWriter
		try {
			pw = new PrintWriter(filename + ".ppm");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes the pixel array to PPM.
	 */
	public void writeToPPM() {
		// Set up the header
		pw.println("P3");
		pw.print("" + pixArrayToConvert.dimensions()[0]);
		pw.println(" " + pixArrayToConvert.dimensions()[1]);
		// Set the default color value
		float defaultColor = 255f;
		pw.println((int) defaultColor);
		// Get the normalized array so that the maximumally visited pixel is
		// black
		float[][] normalizedArray = pixArrayToConvert.normalizedArray();
		// Loop through and print each pixel to the PPM
		for (int i = 0; i < normalizedArray.length; i++) {
			for (int j = 0; j < normalizedArray[i].length; j++) {
				// Subtract from the default color so that the maximum pixel is
				// on the opposite end of the spectrum
				int colorval = (int) (defaultColor - normalizedArray[i][j] * defaultColor);
				// Do the printing
				pw.println("" + colorval + " " + colorval + " " + colorval);
			}
		}
		pw.close();
	}
}
