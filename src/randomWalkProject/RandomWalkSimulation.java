package randomWalkProject;
import java.util.Random;

/**
 * This class performs a random walk simulation in 2-dimensional space. Future
 * functionality will allow for a 1D simulation. Any number of runs can be
 * performed and each run is overlaid on top of previous runs.
 * 
 * @author Andrew Daniel
 *
 */
public class RandomWalkSimulation {

	PixelArray pixArray;

	/**
	 * Public constructor
	 * 
	 * @param pixWidth
	 *            the width of the simulation space in pixels
	 * @param pixHeight
	 *            the height of the simulation space in pixels
	 */
	public RandomWalkSimulation(int pixWidth, int pixHeight) {
		pixArray = new PixelArray(pixWidth, pixHeight);
	}

	/**
	 * Get the PixelArray object representing the array of pixels
	 * 
	 * @return the pixel array
	 */
	public PixelArray getPixelArray() {
		return pixArray;
	}

	/**
	 * Run the simulation, starting at the center of the 2D array each time.
	 * 
	 * @param numRuns
	 *            how many simulations to run on the pixel array.
	 */
	public void runSimulation(int numRuns) {

		Random rand = new Random(System.nanoTime());
		// Summary variables
		float totalTime = 0f;
		float totalSteps = 0f;
		// Run a number of different runs
		for (int i = 1; i <= numRuns; i++) {
			System.out.println("Run: " + i);
			// Set the starting position to be the center of the array
			pixArray.setStartingPosition(pixArray.dimensions()[0] / 2, pixArray.dimensions()[1] / 2);
			// Do the run
			float[] results = doRun(rand);
			// Get the results and add them to total time and steps
			totalSteps += results[0];
			totalTime += results[1];
		}
		// Print the summary of the overall simulation
		System.out.println("\nSimulation summary: " + "\nRuns: " + numRuns + "\nTotal time: "
				+ totalTime + "ms\nTotal steps: " + totalSteps + "\nAverage time taken: "
				+ (totalTime / numRuns) + "ms\nAverage steps: " + ((int) (totalSteps / numRuns)));
	}

	/**
	 * Does one run of the random walk simulation
	 * 
	 * @param rand
	 *            the random number generator
	 * @return a 2 element array containing the number of steps as the first
	 *         element, and the time taken as the second element
	 */
	private float[] doRun(Random rand) {
		double time = System.nanoTime();
		int stepCount = 0;
		// Loop until the pixel array cursor touches a boundary
		while (true) {
			// Get the random number
			int randInt = rand.nextInt(4);
			// Move based on what the random number is
			if ( randInt == 0 ) {
				if ( pixArray.moveUp() )
					break;
			}
			else if ( randInt == 1 ) {
				if ( pixArray.moveRight() )
					break;
			}
			else if ( randInt == 2 ) {
				if ( pixArray.moveDown() )
					break;
			}
			else {
				if ( pixArray.moveLeft() )
					break;
			}
			stepCount++;
		}
		// Get the time taken and print the results of the run
		time = System.nanoTime() - time;
		time /= 1000000f;
		printRunResults(time, stepCount);

		float[] results = { (float) stepCount, (float) time };
		return results;
	}

	/**
	 * Print the results of the run
	 * 
	 * @param time
	 *            the time taken this run
	 * @param stepCount
	 *            how many steps were taken in this run
	 */
	private void printRunResults(double time, int stepCount) {
		System.out.println("Dimensions: " + pixArray.dimensions()[0] + "x"
				+ pixArray.dimensions()[1] + "\nStarting point: ("
				+ pixArray.getStartingPosition()[0] + ", " + pixArray.getStartingPosition()[1]
				+ ")\nTime taken: " + time + "ms\nSteps taken: " + stepCount);
	}
}
