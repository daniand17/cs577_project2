public class Listnode<T> {

	private T data;

	// Number of times this node has been accessed
	private int count = 0;

	private Listnode<T> next;
	private Listnode<T> previous;

	/**
	 * @return the next
	 */
	public Listnode<T> getNext() {
		return next;
	}

	/**
	 * @param next
	 *            the next to set
	 */
	public void setNext(Listnode<T> next) {
		this.next = next;
	}

	/**
	 * @return the previous
	 */
	public Listnode<T> getPrevious() {
		return previous;
	}

	/**
	 * @param previous
	 *            the previous to set
	 */
	public void setPrevious(Listnode<T> previous) {
		this.previous = previous;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	
	public void incrementCount(){
		count++;
	}
}
