public class SplayTree<T extends Comparable<T>> {

	Treenode<T> root;

	public void add(T t) {
		if ( root == null )
			root = new Treenode<T>(t);
	}

	private void add(T t, Treenode<T> root) {
		if ( root.getData().compareTo(t) > 0 ) {
			if ( root.getLeftChild() == null ) {
				root.setLeftChild(new Treenode<T>(t));
			}
			else
				add(t, root.getLeftChild());
		}
		else {
			if ( root.getData().compareTo(t) < 0 ) {
				if ( root.getRightChild() == null )
					root.setRightChild(new Treenode<T>(t));
			}
			else
				add(t, root.getRightChild());
		}
	}
}

class Treenode<T> {

	private T data;
	private Treenode<T> leftChild;
	private Treenode<T> rightChild;

	public Treenode(T data) {
		setLeftChild(null);
		setRightChild(null);
		this.data = data;
	}

	/**
	 * @return the leftChild
	 */
	public Treenode<T> getLeftChild() {
		return leftChild;
	}

	/**
	 * @param leftChild
	 *            the leftChild to set
	 */
	public void setLeftChild(Treenode<T> leftChild) {
		this.leftChild = leftChild;
	}

	/**
	 * @return the rightChild
	 */
	public Treenode<T> getRightChild() {
		return rightChild;
	}

	/**
	 * @param rightChild
	 *            the rightChild to set
	 */
	public void setRightChild(Treenode<T> rightChild) {
		this.rightChild = rightChild;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

}
