public interface IList<T> {

	public void add(T t);

	public boolean contains(T t);

	public T removeAt(int index);

	public T getIndex(int index);

	public T findItem(T t);

	public int size();

	public String toString();
}
