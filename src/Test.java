public class Test {

	private static int testCount = 0;

	public static boolean assertTrue(boolean value) {
		testCount++;
		if ( value ) {
			System.out.println("Test " + testCount + ": True");
			return true;
		}
		else {
			System.out.println("Test " + testCount + ": False");
			return false;
		}
	}

	public static void testCountList() {
		CountList<Integer> countList = new CountList<Integer>();
		countList.add(5);
		assertTrue(countList.size() == 1);
		assertTrue(countList.contains(5));
		assertTrue(countList.findItem(5) != null);
		assertTrue(countList.removeAt(0) == 5);
		assertTrue(!countList.contains(5));
		assertTrue(countList.size() == 0);
		countList.add(10);
		countList.add(20);
		countList.toString();
		assertTrue(countList.getIndex(1) != 10);
		countList.toString();
		assertTrue(countList.getIndex(1) == 10);
		assertTrue(countList.getIndex(0) == 20);
		countList.toString();
		assertTrue(countList.findItem(10) != null);
		assertTrue(countList.findItem(10) != null);
		assertTrue(countList.getIndex(0) == 10);
		countList.toString();
		countList.removeAt(0);
		countList.removeAt(0);
		assertTrue(countList.size() == 0);
		for (int i = 0; i < 10; i++)
			countList.add(i);
		assertTrue(countList.size() == 10);
		countList.toString();
		assertTrue(countList.findItem(9) != null);
		assertTrue(countList.getIndex(0) == 9);
		countList.toString();
		assertTrue(countList.findItem(5) != null);
		countList.toString();
		assertTrue(countList.getIndex(1) == 5);
	}

	public static void testMTFList() {
		MoveToFrontList<Integer> intlist = new MoveToFrontList<Integer>();
		assertTrue(intlist.size() == 0);
		intlist.add(5);
		assertTrue(intlist.contains(5));
		assertTrue(intlist.size() == 1);
		Integer theInt = intlist.removeAt(0);
		assertTrue(intlist.size() == 0);
		assertTrue(theInt.intValue() == 5);
		System.out.println(intlist.toString());
		intlist.add(7);
		intlist.add(8);
		System.out.println(intlist.toString());
		assertTrue(intlist.removeAt(1) == 7);
		assertTrue(intlist.removeAt(2) == null);
		assertTrue(intlist.size() == 1);
		assertTrue(intlist.removeAt(0) == 8);
		assertTrue(intlist.findItem(5) == null);
		for (int i = 0; i < 10; i++)
			intlist.add(i);
		assertTrue(intlist.size() == 10);
		System.out.println(intlist.toString());
		assertTrue(intlist.findItem(7) == 7);
		System.out.println(intlist.toString());
		assertTrue(intlist.getIndex(0) == 7);
	}

	public static void testTransposeList() {
		TransposeList<Integer> translist = new TransposeList<Integer>();
		assertTrue(translist.size() == 0);
		translist.add(5);
		assertTrue(translist.size() == 1);
		assertTrue(translist.contains(5));
		assertTrue(!translist.contains(-1));
		translist.toString();
		assertTrue(translist.removeAt(0) == 5);
		translist.toString();
		assertTrue(translist.removeAt(1) == null);
		for (int i = 0; i < 10; i++)
			translist.add(i);
		assertTrue(translist.size() == 10);
		translist.toString();
		assertTrue(translist.getIndex(1) == 1);
		translist.toString();
		assertTrue(translist.getIndex(9) == 9);
		assertTrue(translist.getIndex(8) == 9);
		translist.toString();
		assertTrue(translist.getIndex(0) == 1);
		translist.toString();
		assertTrue(translist.getIndex(0) == 1);
		assertTrue(translist.getIndex(1) == 0);
		assertTrue(translist.getIndex(0) == 0);
		translist.toString();
		assertTrue(translist.findItem(7) == 7);
		translist.toString();
		assertTrue(translist.findItem(8) == 8);
		translist.toString();
	}
}
