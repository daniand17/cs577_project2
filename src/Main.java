import randomWalkProject.ArrayToPPMConverter;
import randomWalkProject.RandomWalkSimulation;

public class Main {

	public static void main(String[] args) {

		// RunRandomWalk();
		Test.testCountList();
		Test.testMTFList();
		Test.testTransposeList();

	}

	public static void RunRandomWalk() {
		int width = 1000, height = 1000;
		// Creates a new simulation with specified size in pixels
		RandomWalkSimulation sim = new RandomWalkSimulation(width, height);
		// Run the simulation passing the # of runs to do
		sim.runSimulation(1);
		// Create the object which will convert the pixel array to a PPM file
		ArrayToPPMConverter ppmConv = new ArrayToPPMConverter(sim.getPixelArray(), "2DHeatmap");
		// Write the data to a PPM file
		ppmConv.writeToPPM();
	}
}
